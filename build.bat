pdflatex -interaction=nonstopmode  document
REM Option -g oder --german für deutscher Zeichensatz
texindy -g document.idx
biber document
makeglossaries document
pdflatex -interaction=nonstopmode document
pdflatex -interaction=nonstopmode document