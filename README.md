# README #

Diese LaTeX-Vorlage ist für den persönlichen Gebrauch erstellt. Sie kann als Vorlage für Bachelor- oder Masterarbeiten, sowie anderer größerer Dokumente verwendet werden.



## Beschreibung der Vorlage

Eine Beschreibung zu dieser Vorlage kann den Beispieldateien im Verzeichnis examples entnommen werden. Die dort verfügbaren Beispieldateien unterscheiden sich lediglich durch die Farbgestaltung. Sie  enthalten eine Beschreibung der Dokumentenstruktur, der möglichkeiten zur Anpassung sowie einige Beispiele denen zu entnehmen ist wie das spätere Dokument aussehen wird.

## Lizenz 
Creative Commons - Attribution-NonCommercial-ShareAlike 4.0 International (
CC BY-NC-SA 4.0) 		

siehe http://creativecommons.org/licenses/by-nc-sa/4.0/
