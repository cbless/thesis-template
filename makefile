# Makefile zum Erstellen eines PDF-Dokuments aus 
# den Latex Datei.

MAINFILE = document
TARGETNAME = document


LATEX 		= pdflatex
BIBTEX		= biber
MAKEINDEX	= texindy
MAKEGLS		= makeglossaries

RM = rm -f 

FEATURES 	= index bib glossaries

INCLUDEDIR 	= includes

FLAGS = -interaction=nonstopmode 

# Da xindy verwendet wird sollte die Option -g oder --german angegeben 
# werden, damit die deutschen Umlaute richtig behandelt werden
MAKEINDEX_OPTIONS = -g

TMP_EXT = aux out log idx ind ilg glg gls glo alg acr acn slg syi syg toc lot lof toc1 ist lol bbl bcf blg glsdefs run.xml xdy

TMP_FILES = $(foreach T, $(TMP_EXT), $(BUILDDIR)/$(TARGETNAME).$(T))

.PHONY: help 

help:
	@echo 
	@echo all           : erst clean dann PDF-Dokument erstellen
	@echo build         : PDF-Dokument erstellen
	@echo clean		    : temporäre Dateien löschen
	@echo index		    : Index erstellen
	@echo bib		    : Literaturverzeichnis erstellen
	@echo glossaries	: Glossar und Abkürzungsverzeichnis erstellen
	@echo 
	@echo Voreingestellungen
	@echo Mainfile 		: $(MAINFILE).tex
	@echo Features 		: $(FEATURES)
	@echo

$(MAINFILE).idx $(MAINFILE).aux: $(MAINFILE).tex
	$(LATEX) $(FLAGS) $(MAINFILE).tex

# Index erstellen mit vorangestelltem Buchstaben vor der Buchstabengruppe
index:  $(MAINFILE).idx
	$(MAKEINDEX) $(MAKEINDEX_OPTIONS) $(MAINFILE).idx

# Glossar erstellen
glossaries: $(MAINFILE).idx
	$(MAKEGLS) $(MAINFILE)
	
# Literaturverzeichnis erstellen
bib: 	$(MAINFILE).idx
	$(BIBTEX) $(MAINFILE)

build: $(FEATURES)
	$(LATEX) $(FLAGS) $(MAINFILE)
	$(LATEX) $(FLAGS) $(MAINFILE)

all: clean build

clean:
	@$(RM) $(foreach T, $(TMP_EXT), $(MAINFILE).$(T))
	@$(RM) $(foreach T, $(TMP_EXT), $(TARGETNAME).$(T))
	@$(RM) $(TARGETNAME)-blx.bib 
	@$(RM) content/*.aux 
	@$(RM) preamble/*.aux
